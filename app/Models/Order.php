<?php

namespace App\Models;

use App\Interfaces\SaveApiObject;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Models
 * @property int $id
 * @property float $total
 * @property float $shipping_total
 * @property Carbon $create_time
 * @property string $timezone
 */
class Order extends Model implements SaveApiObject
{
    /**
     * @param array $data
     * @return Order|mixed
     */
    public static function getObjectFromAPi(array $data)
    {
        $order = Order::find($data['id']['value']);

        if(!$order) $order = new Order();

        $order->id = $data['id']['value'];
        $order->total = $data['total']['value'];
        $order->shipping_total = $data['shipping_total']['value'];
        $order->create_time = Carbon::createFromTimestamp($data['create_time']['value']);
        $order->timezone = $data['timezone']['value'];

        return $order;
    }

    /**
     * @return array
     */
    public function toArrayForView(): array
    {
        return [
            'id' => $this->id,
            'total' => $this->total,
            'shipping_total' => $this->shipping_total,
            'create_time' => $this->create_time->timestamp,
            'timezone' => $this->timezone
        ];
    }
}
