<?php

namespace App\Models;

use App\Interfaces\SaveApiObject;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 * @property string $title
 * @property string $sku
 * @property string $image
 * @property string $image_encoding
 * @property string $view_encoding
 */
class Product extends Model implements SaveApiObject
{
    protected $primaryKey = 'sku';

    /**
     * @param array $data
     * @return Product|mixed
     */
    public static function getObjectFromAPi(array $data)
    {
        $product = Product::find($data['SKU']['value']);

        if(!$product) $product = new Product();

        $product->sku = $data['SKU']['value'];
        $product->title = $data['title']['value'];
        $product->image = $data['image']['value'];
        $product->image_encoding = $data['image']['encoding'];

        return $product;
    }

    /**
     * @return array
     */
    public function toArrayForView(): array
    {
        return [
            'sku' => $this->sku,
            'title' => $this->title,
            'image' => $this->image,
            'encoding' => $this->view_encoding,
        ];
    }

    /**
     * @return string
     */
    public function getViewEncodingAttribute(): string
    {
        $array = explode(';', $this->image_encoding);
        $array = array_reverse($array);

        return implode(';', $array);
    }
}
