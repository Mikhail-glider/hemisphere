<?php


namespace App\Interfaces;


interface SaveApiObject
{
    /**
     * @param array $data
     * @return mixed
     */
    public static function getObjectFromAPi(array $data);

    /**
     * @return array
     */
    public function toArrayForView(): array;

    /**
     * @param array $options
     * @return mixed
     */
    public function save(array $options = []);
}
