<?php


namespace App\Classes;


use App\Interfaces\SaveApiObject;
use Exception;

class ApiObjectFabric
{
    /**
     * @param string $type
     * @param array $data
     * @return SaveApiObject|null
     */
    public function getObject(string $type, array $data): ?SaveApiObject
    {
        try {
            $class = 'App\Models\\' . ucfirst($type);

            return $class::getObjectFromAPi($data);
        } catch (Exception $exception) {
            return null;
        }
    }
}
