<?php


namespace App\Classes;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Exception;

class Api
{
    const API_BASE_URL = "https://apptest.wearepentagon.com/devInterview/API/en";
    const API_AUTH_URL = "/access-token";
    const API_INFO_URL = "/get-random-test-feed";

    /**
     * @var Client
     */
    protected $client;

    /**
     * Api constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @return InfoResponse
     * @throws GuzzleException
     */
    public function getInfo(): InfoResponse
    {
        $response = $this->client->get(static::API_BASE_URL . static::API_INFO_URL, [
            'headers' => [
                'Authorization' => 'Bearer ' . Auth::token()
            ]
        ]);

        $rawResponse = $response->getBody()->getContents();
        $rawResponse = trim($rawResponse, '"');

        return new InfoResponse($rawResponse);
    }

    /**
     * @return string
     * @throws GuzzleException
     */
    public function getToken(): string
    {
        $response = $this->client->post(static::API_BASE_URL . static::API_AUTH_URL, [
            'form_params' => [
                "client_id" => env('API_USER', ''),
                "client_secret" => env('API_PASS', '')
            ]
        ]);

        if ($response->getStatusCode() !== 200)
            throw new Exception("Something wrong with request for token Auth");

        $responseData = json_decode($response->getBody()->getContents());

        return $responseData->access_token ?? '';
    }
}
