<?php


namespace App\Classes;


use App\Models\Settings;
use Carbon\Carbon;

class Auth
{
    const API_TOKEN_KEY_FIELD_NAME_IN_DB = "token";
    const TOKEN_LIFE_TIME_HOURS = 1;

    /**
     * @var Api
     */
    protected $api;

    /**
     * Auth constructor.
     */
    public function __construct()
    {
        $this->api = new Api();
    }

    /**
     * @return string
     */
    static function token(): string
    {
        return (new static())->getToken();
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        if ($tokenFromDb = $this->getTokenFromDb())
            return $tokenFromDb;
        else
            return $this->getTokenFromApi();
    }

    /**
     * @return string|null
     */
    protected function getTokenFromDb(): ?string
    {
        $settingModel = Settings::where('key', static::API_TOKEN_KEY_FIELD_NAME_IN_DB)->first();
        if ($settingModel) {
            if ($this->isTokenTimeMoreOneHour($settingModel))
                return null;
            else
                return $settingModel->value;
        }

        return null;
    }

    /**
     * @param Settings $model
     * @return bool
     */
    protected function isTokenTimeMoreOneHour(Settings $model):bool
    {
        return $model->updated_at->diffInHours(Carbon::now()) >= static::TOKEN_LIFE_TIME_HOURS;
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getTokenFromApi(): string
    {
        $token = $this->api->getToken();
        $this->saveTokenInDb($token);

        return $token;
    }

    /**
     * @param string $token
     */
    protected function saveTokenInDb(string $token): void
    {
        $settingModel = Settings::where('key', static::API_TOKEN_KEY_FIELD_NAME_IN_DB)
            ->first();
        if (is_null($settingModel)) {
            $settingModel = new Settings();
            $settingModel->key = static::API_TOKEN_KEY_FIELD_NAME_IN_DB;
        }

        $settingModel->value = $token;
        $settingModel->save();
    }
}
