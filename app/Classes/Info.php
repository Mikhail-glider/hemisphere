<?php


namespace App\Classes;


use App\Interfaces\SaveApiObject;
use GuzzleHttp\Exception\GuzzleException;

class Info
{
    /**
     * @var Api
     */
    protected $api;

    /**
     * Info constructor.
     */
    public function __construct()
    {
        $this->api = new Api();
    }

    /**
     * @return SaveApiObject
     * @throws GuzzleException
     */
    static function getObject(): SaveApiObject
    {
        return (new static())->getObjectInfo();
    }

    /**
     * @return SaveApiObject
     * @throws GuzzleException
     */
    public function getObjectInfo(): SaveApiObject
    {
        $response = $this->api->getInfo();
        $object = (new ApiObjectFabric)->getObject($response->getType(), $response->getData());
        $object->save();

        return $object;
    }
}
