<?php


namespace App\Classes;



class InfoResponse
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var array
     */
    protected $data;

    /**
     * InfoResponse constructor.
     * @param string $rawData
     */
    public function __construct(string $rawData)
    {
        $data_array = explode('||', $rawData);
        $this->type = $this->getTypeFromRawData($data_array[0]);
        $this->data = $this->processData($data_array);
    }

    /**
     * @param array $array
     * @return array
     */
    protected function processData(array $array): array
    {
        $first_element = array_shift($array);
        $result_array = $this->processFirstElement($first_element);

        foreach ($array as $item) {
            $temp_array = $this->getKeyAndEncoding($item);
            $result_array[$this->getKey($item)] = $temp_array;
        }

        return $result_array;
    }

    /**
     * @param string $string
     * @return array
     */
    protected function processFirstElement(string $string): array
    {
        $data_array = explode(":", $string);

        return [
            $this->getKey($data_array[1]) => $this->getKeyAndEncoding($data_array[1])
        ];
    }

    /**
     * @param string $string
     * @return array
     */
    protected function getKeyAndEncoding(string $string): array
    {
        $array = [
            'value' => $this->getValue($string),
        ];

        if ($encoding = $this->getEncoding($string)) {
            $array['encoding'] = $encoding;
        }

        return $array;
    }

    /**
     * @param string $data
     * @return string|null
     */
    protected function getKey(string $data): ?string
    {
        $res = null;
        preg_match('/^[^{]*/', $data, $res);
        preg_match('/^[^\\\\]*/', $res[0] ?? null, $res);

        return $res[0] ?? null;
    }

    /**
     * @param string $data
     * @return string|null
     */
    protected function getEncoding(string $data): ?string
    {
        $res = null;
        if (preg_match('/^[^{]*/', $data, $res)) {
            $split_result = preg_split('/^[^\\\\]*/', $res[0]);
            if (empty($split_result[1])) return null;

            return substr($split_result[1], 2) ?? null;
        }

        return null;
    }

    /**
     * @param string $data
     * @return string|null
     */
    protected function getValue(string $data): ?string
    {
        $res = null;
        preg_match('/{([^}]*)}/', $data, $res);

        return $res[1] ?? null;
    }

    /**
     * @param string $data
     * @return string
     */
    protected function getTypeFromRawData(string $data): string
    {
        $data_array = explode(":", $data);

        return $data_array[0];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }


}
