<?php

namespace App\Http\Controllers;

use App\Classes\Info;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

class MainController extends Controller
{
    public function index()
    {
        try {
            $object = Info::getObject();

            return view('info', ['data' => $object->toArrayForView()]);
        } catch (GuzzleException $exception) {
            return response('Error with getting info from api', 502);
        } catch (Exception $exception) {
            return response('Application error', 502);
        }
    }
}
