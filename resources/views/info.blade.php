<ul>
    @foreach ($data as $key => $item)
        <li><strong>{{ $key }}</strong> -
            @if($key == 'image')
                <img src="data:{{ $data['encoding'] }},{{ stripslashes($item) }}" alt="Image">
            @else
                {{ stripslashes($item) }}
            @endif
        </li>
    @endforeach
</ul>
